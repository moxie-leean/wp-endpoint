<?php namespace Leean\Endpoints;

/**
 * Defines the filters used on the Endpoint class.
 *
 * @package Leean/Endpoints
 */
class Filters {
	const API_NAMESPACE = 'ln_endpoints_api_namespace';
	const API_VERSION = 'ln_endpoints_api_version';
	const API_DATA = 'ln_endpoints_data';

	const ITEM_FORMAT = 'ln_endpoints_collection_item';
}
